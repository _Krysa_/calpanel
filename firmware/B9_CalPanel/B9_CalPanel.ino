/*
 * Firmware v0.1 for the B9 Cal Panel hardware rev a
 * 
 * This firmware is designed for Arduino nano form factor devices and supports a 5V 270 degree servo
 * motor (DS3218MG), a 5V WS2812B compatible LED string and a KY-040 compatible rotary encoder.
 * 
 * The hardware runs entirely off of a USB3.0 or later interface, with no external power needed (see note!)
 * 
 * !!!!!!!!!!!!!!!!!!!
 * !!!! IMPORTANT !!!!
 * !!!!!!!!!!!!!!!!!!!
 * 
 * The Arduino nano has a reverse voltage blocking diode which only supports a small amount of current.
 * In order to power the servo and LED strip from USB, the diode must either be bypassed by shorting
 * both sides of it together or by connecting the 5V for the servo and LED to the anode of the diode.
 * 
 * Ignoring this will likely blow the diode, you have been warned!
 * 
 * !!!!!!!!!!!!!!!!!!!
 * 
 * Third-party Library Requirements
 * 
 * Encoder
 * FastLED
 * ServoEasing
 * EEPROM-Storage
 * 
 * USB Serial Interface Protocol
 * 
 * LED x    => Set LED brightness to 'x', 0 <= x <= 255 (a value of 0 turns the LED off)
 *             Returns 'LED x' when complete, brightness is saved if non-zero
 * PAN x    => Open (0) or Close (1) the panel flap (non-zero value assumes close command)
 *             Returns 'PAN x' when complete
 * VER      => Returns 'B9 CalPanel v0.1a'
 * 
 * These can also be sent without a paramter which will query the saved value
 * 
 * Manual Operation
 * 
 * Short press (< 3 seconds) on the rotary encoder => Toggle the LED state
 * Adjust rotary encoder => Adjust the LED brightness (only valid when LED in in the On state), brightness is saved for manual operation
 * Long press (> 3 seconds) on the rotary encoder => Toggle the panel state open/close
 * 
 * Note: Once a panel movement is initiated, further control is blocked until it is complete
 * 
 * Default Device Pinout
 * 
 * KY-040 Rotary Encoder
 * 
 * CLK  =>  D3
 * DT   =>  D2
 * SW   =>  D4
 * +    =>  3V3
 * GND  =>  GND
 * 
 * DS3218MG Servo
 * 
 * +V   =>  5V (red)
 * GND  =>  GND (black)
 * PWM  =>  D5 (white)
 * 
 * WS2812B Strip
 * 
 * +V   =>  5V (red)
 * GND  =>  GND (white)
 * CTRL =>  D11 (green)
 */
 
#include <stdint.h>
#include <stdio.h>
#include <Encoder.h>
#include <FastLED.h>
#include <ServoEasing.h>
#include <EEPROM-Storage.h>

// version info
#define VERSION             "B9 CalPanel v0.1a"

// pins used for encoder
// for best performance, use inputs that support interrupts for the encoder quadrature signals
#define ENC_PIN_DT          2
#define ENC_PIN_CLK         3
#define ENC_SW              4

// other defines for encoder
#define ENC_DEBOUNCE_TIME   25    // this might need tuning to prevent double detection on your hardware
#define ENC_LONG_PRESS_TIME 3000  // wait this long to determine a "long" button press
#define ENC_PRESSED         0     // button is pulled high, lo when pressed

// pins used for LED
// for best performance, use the MOSI pin for LED data
#define LED_PIN             11

// other defines for LED
#define NUM_LEDS                60    // the panel is 30mm larger in diameter than the OTA definition in CAD
                                      // number of LEDs = 144 * (panel dia in m) * pi
#define CHIPSET                 WS2812B
#define COLOR_ORDER             GRB
#define LED_EEPROM_UPDATE_TIME  5000  // wait this long before updating EEPROM so we don't thrash it
#define LED_MAX_BRIGHTNESS      255
// LED calibration - tweak the RGB values for proper white balance NB: white balance is non-linear with brightness
// these values are reasonable starting points
#define LED_RED_CAL             0x0ffl
#define LED_GREEN_CAL           0x0f0l
#define LED_BLUE_CAL            0x0b0l
#define LED_CALIBRATION         (LED_GREEN_CAL << 16) | (LED_RED_CAL << 8) | LED_BLUE_CAL

// pins used for servo
// for best performance, use a pin that supports hardware PWM
#define SERVO_PIN           5

// other defines for servo
#define DS3218_0_DEGREES    500             // 500us = 0 degrees
#define DS3218_270_DEGREES  2500            // 2500us = 270 degrees
#define PANEL_OPEN          (uint8_t)0      // the servo driver uses 0 and 180 for min,max values regardless if a 180 or 270 servo
#define PANEL_CLOSED        (uint8_t)180    // these are mapped linearly onto the max,min supplied in the servo instance
#define PANEL_RATE          4               // smaller values = faster

// defines for USB serial interface
#define SERIAL_SPEED        115200
#define SERIAL_MAX_INPUT    8

// non-volatile memory
EEPROMStorage<uint8_t> panelState(0, 0xff);
EEPROMStorage<uint8_t> panelBrightness(2, 0xff);

// globals
CRGB leds[NUM_LEDS];
Encoder encoder(ENC_PIN_DT, ENC_PIN_CLK);
ServoEasing servo;
unsigned long buttonPressTime, encoderAdjustTime = 0;
bool buttonPressed, longPress, shortPress, supressButton, updateBrightness, LEDOn = false;
bool lastButton = true;
int tempBrightness;
long lastEncoder = 0;

void setup() {
  // set encoder inputs
  pinMode(ENC_PIN_DT, INPUT);
  pinMode(ENC_PIN_CLK, INPUT);
  pinMode(ENC_SW, INPUT_PULLUP);

  // set up LED string
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(LED_CALIBRATION);

  // set initial brightness to last saved value, default to 0xff (full brightness)
  for(int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CHSV(0, 0, panelBrightness);
  }

  // save panel brightness to temp var, we will normally muck with this one until the user
  // stops fiddling with it for a while so we don't thrash the EEPROM too much
  tempBrightness = panelBrightness;

  // set up servo
  // if we have never saved a panel state (first run), drive the motor to the closed 
  // position to allow for aligning the motor and panel position during assembly
  // if we already have a saved state, use that for the initial motor positioning
  // so that we don't unexpectedly open or close the panel when we are first powered on
  servo.attach(SERVO_PIN, DS3218_0_DEGREES, DS3218_270_DEGREES);
  servo.setSpeed(180 / PANEL_RATE);
  servo.setEasingType(EASE_SINE_IN_OUT);

  if((uint8_t)panelState == 0xff) {
    servo.write(PANEL_CLOSED);
    panelState = PANEL_CLOSED;
  }
  else {
    servo.write(panelState);
  }

  Serial.begin(SERIAL_SPEED);
}

void handleButton(void) {
  bool button = digitalRead(ENC_SW);

  if(button != lastButton) {
    buttonPressTime = millis();
  }

  // do nothing until debounce has elapsed
  if((millis() - buttonPressTime) > ENC_DEBOUNCE_TIME) {
    if(button == ENC_PRESSED) {
      // encoder button is pressed, only flag it if we are not waiting for long press to clear
      if(!buttonPressed && !supressButton) {
        buttonPressed = true;
      }

      // check if long press
      if((millis() - buttonPressTime) > ENC_LONG_PRESS_TIME && !supressButton) {
        longPress = true;
        buttonPressed = false;
        supressButton = true;   // set this to make sure the long press clears before handling any new presses
      }
    }
    else {
      // encoder button is not pressed
      // clear button state
      if(buttonPressed && !supressButton) {
        buttonPressed = false;
        shortPress = true;
      }
    
      // wait until user releases button from a long press to re-enable normal button handling
      if(supressButton) {
        supressButton = false;
      }
    }
  }

  lastButton = button;
}

void handleEncoder() {
  long delta = encoder.read() - lastEncoder;

  if(delta != 0) {
    // save the new encoder accumulated value
    lastEncoder += delta;

    // do nothing except to save the accumulated encoder pos unless the LED is on
    if(LEDOn) {
      // calculate new brightness, keep within limits
      tempBrightness += delta;

      if(tempBrightness < 0) {
        tempBrightness = 0;
      }

      if(tempBrightness > LED_MAX_BRIGHTNESS) {
        tempBrightness = LED_MAX_BRIGHTNESS;
      }

      // update the panel
      for(int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CHSV(0, 0, tempBrightness);
      }

      FastLED.show();

      // save the adjustment timestamp
      encoderAdjustTime = millis();
      updateBrightness = true;
    }
  }

  // check if we need to save the brightness
  if(updateBrightness && (millis() - encoderAdjustTime) > LED_EEPROM_UPDATE_TIME) {
    panelBrightness = tempBrightness;
    updateBrightness = false;
    Serial.print("LED ");
    Serial.print(tempBrightness);
    Serial.print("\n");
  }
}

void toggleLED(void) {
  uint8_t brightness;

  if(LEDOn) {
    brightness = 0;
    LEDOn = false;
  }
  else {
    brightness = tempBrightness;
    LEDOn = true;
  }

  for(int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CHSV(0, 0, brightness);
  }

  FastLED.show();

  Serial.print("LED ");
  Serial.print(brightness);
  Serial.print("\n");
}

void togglePanel(void) {
  if(panelState == PANEL_OPEN) {
    servo.easeTo(PANEL_CLOSED);
    panelState = PANEL_CLOSED;
  }
  else {
    servo.easeTo(PANEL_OPEN);
    panelState = PANEL_OPEN;
  }

  Serial.print("PAN ");
  Serial.print((panelState == PANEL_OPEN)?"0":"1");
  Serial.print("\n");
}

bool isQuery(char* command) {
  return (command[4] == 0);
}

void processCommand(char* command) {
  // LED command
  if(command[0] == 'L' && command[1] == 'E' && command[2] == 'D') {
    if(isQuery(command)) {
      Serial.print("LED ");
      Serial.print(tempBrightness);
      Serial.print("\n");
    }
    else {
      int brightness = atoi(&command[4]);

      // limit brightness to 0-LED_MAX_BRIGHTNESS
      if(brightness < 0) {
        brightness = 0;
      }
      if(brightness > LED_MAX_BRIGHTNESS) {
        brightness = LED_MAX_BRIGHTNESS;
      }

      if(brightness > 0) {
        // save LED brightness and toggle LED on
        tempBrightness = panelBrightness = brightness;
        LEDOn = false;
        toggleLED();
      }
      else {
        // toggle LED off
        tempBrightness = 0;
        LEDOn = true;
        toggleLED();
      }
    }
  }
  // PAN command
  else if(command[0] == 'P' && command[1] == 'A' && command[2] == 'N') {
    if(!isQuery(command)) {
      int state = atoi(&command[4]);

      // limit to valid states
      if(state < 0) {
        state = 0;
      }
      if(state > 1) {
        state = 1;
      }

      if(state) {
        servo.easeTo(PANEL_CLOSED);
        panelState = PANEL_CLOSED;
      }
      else {
        servo.easeTo(PANEL_OPEN);
        panelState = PANEL_OPEN;
      }
    }

    Serial.print("PAN ");
    Serial.print((panelState == PANEL_OPEN)?"0":"1");
    Serial.print("\n");
  }
  // VER command
  else if(command[0] == 'V' && command[1] == 'E' && command[2] == 'R') {
    Serial.print(VERSION);
    Serial.print("\n");
  }

  for(int i = 0;i < SERIAL_MAX_INPUT;i++) {
    command[i] = 0;
  }
}

void processSerial(char dataByte) {
  static uint8_t commandPos = 0;
  static char command[SERIAL_MAX_INPUT] = {0,0,0,0,0,0};

  switch(dataByte) {
    case '\n':
      command[commandPos] = 0;
      processCommand(command);
      commandPos = 0;
      break;
    case '\r':
      break;
    default:
      if(commandPos < (SERIAL_MAX_INPUT - 1)) {
        command[commandPos++] = dataByte;
      }
  }
}

void loop() {
  while(Serial.available() > 0) {
    processSerial(Serial.read());
  }
  handleButton();
  handleEncoder();

  if(shortPress) {
    toggleLED();
    shortPress = false;
  }

  if(longPress) {
    togglePanel();
    longPress = false;
  }
}
