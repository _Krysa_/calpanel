B9 CalPanel v0.1a

Instructions for printing and assembly at https://docs.google.com/document/d/1VV9CunwyOTu5k_3kxSH5o6iANpJEpwg5Pu7IEfHsnA0/edit?usp=sharing

ASCOM driver at https://bitbucket.org/\_Krysa\_/calpanel/downloads/

Stand-off electronics box for use with Arduinos with pins soldered designed by Machinza.
